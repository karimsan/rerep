import * as types from '../actions/actionTypes';

const initialState = {
  isFetching: true,
  list: [],
};

export default function commentsReducer(state = initialState, action) {
  switch (action.type) {
    case `${types.COMMENTS_LOAD}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.COMMENTS_LOAD}_FULFILLED`:
      return {
        isFetching: false,
        list: action.payload.data.comments,
      };

    case `${types.COMMENTS_LOAD}_REJECTED`:
      return Object.assign({}, state, {
        isFetching: false,
        list: [],
      });

    case `${types.COMMENTS_CREATE}_FULFILLED`:
      return Object.assign({}, state, {
        list: state.list.concat([action.payload.data.comment]),
      });

    case `${types.LOGOUT}`:
      return Object.assign({}, initialState);

    default:
      return state;
  }
}
