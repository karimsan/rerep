import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';
import repairs from './repairsReducer';
import repair from './repairReducer';
import users from './usersReducer';
import user from './userReducer';
import auth from './authReducer';
import comments from './commentsReducer';

const repairApp = combineReducers({
  repairs,
  repair,
  users,
  user,
  auth,
  comments,
  loadingBar,
});

export default repairApp;
