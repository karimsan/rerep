import axios from 'axios';
import * as types from '../actions/actionTypes';
import { setToken, cleanToken } from '../token';


const initialState = {
  isFetching: false,
  user: null,
};

function saveToken(token) {
  axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  setToken(token);
}

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case `${types.LOGIN}_PENDING`:
      delete axios.defaults.headers.common.Authorization;
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.LOGIN}_FULFILLED`:
      saveToken(action.payload.data.access_token);
      return Object.assign({}, state, {
        isFetching: false,
      });

    case `${types.LOGIN}_REJECTED`:
      return Object.assign({}, state, {
        isFetching: false,
        user: null,
      });

    case `${types.ME}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.ME}_FULFILLED`:
      return {
        isFetching: false,
        user: action.payload.data,
      };

    case `${types.ME}_REJECTED`:
      if (action.payload.response && action.payload.response.status === 401) {
        delete axios.defaults.headers.common.Authorization;
        cleanToken();
      }

      return {
        isFetching: false,
        user: null,
      };

    case `${types.REGISTER}_PENDING`:
      delete axios.defaults.headers.common.Authorization;
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.REGISTER}_FULFILLED`:
      saveToken(action.payload.data.access_token);
      return Object.assign({}, state, {
        isFetching: false,
      });

    case `${types.REGISTER}_REJECTED`:
      return Object.assign({}, state, {
        isFetching: false,
        user: null,
      });

    case `${types.LOGOUT}`:
      delete axios.defaults.headers.common.Authorization;
      cleanToken();
      return Object.assign({}, state, {
        isFetching: false,
        user: null,
      });

    default:
      return state;
  }
}
