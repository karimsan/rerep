import * as types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  user: null,
};


export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case `${types.USER_LOAD_ONE}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.USER_LOAD_ONE}_FULFILLED`:
      return {
        isFetching: false,
        user: action.payload.data.user,
      };

    case `${types.USER_LOAD_ONE}_REJECTED`:
      return Object.assign({}, state, {
        isFetching: false,
        user: null,
      });

    case `${types.LOGOUT}`:
      return Object.assign({}, initialState);

    default:
      return state;
  }
}
