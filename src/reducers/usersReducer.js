import * as types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  list: [],
};
export default function usersReducer(state = initialState, action) {
  switch (action.type) {
    case `${types.USERS_LOAD_LIST}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.USERS_LOAD_LIST}_FULFILLED`:
      return {
        isFetching: false,
        list: action.payload.data.users,
      };

    case `${types.USERS_LOAD_LIST}_REJECTED`:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case `${types.USER_CREATE}_FULFILLED`:
      return Object.assign({}, state, {
        list: state.list.concat([action.payload.data.user]),
      });

    case `${types.USER_DELETE}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.USER_DELETE}_FULFILLED`:
      return Object.assign({}, state, {
        isFetching: false,
        list: state.list.filter(user => (user.id !== action.meta.id)),
      });

    case `${types.USER_DELETE}_REJECTED`:
      if (action.payload.response && action.payload.response.status === 404) {
        return Object.assign({}, state, {
          isFetching: false,
          list: state.list.filter(user => (user.id !== action.meta.id)),
        });
      }
      return Object.assign({}, state, {
        isFetching: false,
      });

    case `${types.USER_UPDATE}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.USER_UPDATE}_FULFILLED`:
      return Object.assign({}, state, {
        isFetching: false,
        list: state.list.map((user) => {
          if (user.id !== action.meta.id) {
            return user;
          }
          return action.payload.data.user;
        }),
      });

    case `${types.USER_UPDATE}_REJECTED`:
      if (action.payload.response && action.payload.response.status === 404) {
        return Object.assign({}, state, {
          isFetching: false,
          list: state.list.filter(user => (user.id !== action.meta.id)),
        });
      }
      return Object.assign({}, state, {
        isFetching: false,
      });

    case `${types.USER_LOAD_ONE}_REJECTED`:
      if (action.payload.response && action.payload.response.status === 404) {
        return Object.assign({}, state, {
          list: state.list.filter(user => (user.id !== action.meta.id)),
        });
      }
      return state;

    case `${types.LOGOUT}`:
      return Object.assign({}, initialState);

    default:
      return state;
  }
}
