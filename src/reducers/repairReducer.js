import * as types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  repair: null,
};


export default function repairReducer(state = initialState, action) {
  switch (action.type) {
    case `${types.REPAIR_LOAD_ONE}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.REPAIR_LOAD_ONE}_FULFILLED`:
      return {
        isFetching: false,
        repair: action.payload.data.repair,
      };

    case `${types.REPAIR_LOAD_ONE}_REJECTED`:
      return Object.assign({}, state, {
        isFetching: false,
        repair: null,
      });

    case `${types.LOGOUT}`:
      return Object.assign({}, initialState);

    default:
      return state;
  }
}
