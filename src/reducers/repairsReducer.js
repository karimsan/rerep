import moment from 'moment';
import * as types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  list: [],
};

const extractDateAndTime = (repair) => {
  const dateTime = {
    date: moment(repair.date).format('YYYY-MM-DD'),
    time: moment(repair.date).format('HH:mm'),
  };
  return Object.assign({}, repair, dateTime);
};

export default function repairsReducer(state = initialState, action) {
  switch (action.type) {
    case `${types.REPAIRS_LOAD_LIST}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.REPAIRS_LOAD_LIST}_FULFILLED`:
      return {
        isFetching: false,
        list: action.payload.data.repairs.map(extractDateAndTime),
      };

    case `${types.REPAIRS_LOAD_LIST}_REJECTED`:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case `${types.REPAIR_CREATE}_FULFILLED`:
      return Object.assign({}, state, {
        list: state.list.concat([extractDateAndTime(action.payload.data.repair)]),
      });

    case `${types.REPAIR_DELETE}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.REPAIR_DELETE}_FULFILLED`:
      return Object.assign({}, state, {
        isFetching: false,
        list: state.list.filter(repair => (repair.id !== action.meta.id)),
      });

    case `${types.REPAIR_DELETE}_REJECTED`:
      if (action.payload.response && action.payload.response.status === 404) {
        return Object.assign({}, state, {
          isFetching: false,
          list: state.list.filter(repair => (repair.id !== action.meta.id)),
        });
      }
      return Object.assign({}, state, {
        isFetching: false,
      });

    case `${types.REPAIR_UPDATE}_PENDING`:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case `${types.REPAIR_UPDATE}_FULFILLED`:
      return Object.assign({}, state, {
        isFetching: false,
        list: state.list.map((repair) => {
          if (repair.id !== action.meta.id) {
            return repair;
          }
          return extractDateAndTime(action.payload.data.repair);
        }),
      });

    case `${types.REPAIR_UPDATE}_REJECTED`:
      if (action.payload.response && action.payload.response.status === 404) {
        return Object.assign({}, state, {
          isFetching: false,
          list: state.list.filter(repair => (repair.id !== action.meta.id)),
        });
      }
      return Object.assign({}, state, {
        isFetching: false,
      });

    case `${types.REPAIR_LOAD_ONE}_REJECTED`:
      if (action.payload.response && action.payload.response.status === 404) {
        return Object.assign({}, state, {
          list: state.list.filter(repair => (repair.id !== action.meta.id)),
        });
      }
      return state;

    case `${types.LOGOUT}`:
      return Object.assign({}, initialState);

    default:
      return state;
  }
}
