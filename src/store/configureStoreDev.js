/* eslint-disable import/no-extraneous-dependencies */
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import promiseMiddleware from 'redux-promise-middleware';
import rootReducer from '../reducers';

export default function configureStore() {
  return createStore(rootReducer, {}, composeWithDevTools(applyMiddleware(
    thunkMiddleware,
    promiseMiddleware(),
    loadingBarMiddleware(),
  )));
}
