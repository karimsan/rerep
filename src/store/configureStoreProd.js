import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import rootReducer from '../reducers';

export default function configureStore() {
  return createStore(rootReducer, {}, applyMiddleware(
    thunkMiddleware,
    promiseMiddleware(),
    loadingBarMiddleware(),
  ));
}
