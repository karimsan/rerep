import React from 'react';
import PropTypes from 'prop-types';
import { Route, withRouter, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Alert from 'react-s-alert';
import * as repairActions from '../actions/repairActions';
import * as userActions from '../actions/userActions';
import * as authActions from '../actions/authActions';
import ManagerNavbar from './common/ManagerNavbar';
import UserNavbar from './common/UserNavbar';
import RepairsPage from './repairs/RepairsPage';
import AddRepairPage from './repairs/AddRepairPage';
import EditRepairPage from './repairs/EditRepairPage';
import { userShape } from '../shapes/index';
import * as roles from './roles/roles';
import RoleContainer from './roles/RoleContainer';
import UserRepairsPage from './repairs/UserRepairsPage';
import UsersPage from './users/UsersPage';
import AddUserPage from './users/AddUserPage';
import EditUserPage from './users/EditUserPage';
import CommentsPage from './comments/CommentsPage';

class AuthedApp extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
    this.props.actions.loadRepairs()
      .catch(() => Alert.error('Load repairs error'));
    if (this.props.user.role === roles.MANAGER) {
      this.props.actions.loadUsers()
        .catch(() => Alert.error('Load users error'));
    }
  }

  logout(event) {
    event.preventDefault();
    this.props.actions.logout();
    Alert.success('Logged out');
  }

  render() {
    const { user } = this.props;
    return (
      <div>
        <RoleContainer role={roles.MANAGER} user={user}>
          <ManagerNavbar logout={this.logout} />
          <Switch>
            <Route exact path="/" component={RepairsPage} />
            <Route path="/repairs/add" component={AddRepairPage} />
            <Route path="/repairs/edit/:id" component={EditRepairPage} />
            <Route exact path="/users" component={UsersPage} />
            <Route path="/users/add" component={AddUserPage} />
            <Route path="/users/edit/:id" component={EditUserPage} />
            <Route path="/comments/:id" component={CommentsPage} />
          </Switch>
        </RoleContainer>
        <RoleContainer role={roles.USER} user={user}>
          <UserNavbar logout={this.logout} />
          <Switch>
            <Route exact path="/" component={UserRepairsPage} />
            <Route path="/comments/:id" component={CommentsPage} />
          </Switch>
        </RoleContainer>
      </div>
    );
  }
}

AuthedApp.propTypes = {
  user: userShape.isRequired,
  actions: PropTypes.shape({
    loadUsers: PropTypes.func,
    loadRepairs: PropTypes.func,
    logout: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.auth.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: Object.assign({},
      bindActionCreators(repairActions, dispatch),
      bindActionCreators(userActions, dispatch),
      bindActionCreators(authActions, dispatch),
    ),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AuthedApp));
