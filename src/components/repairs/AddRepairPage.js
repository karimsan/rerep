import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Alert from 'react-s-alert';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import * as repairActions from '../../actions/repairActions';
import RepairForm from './RepairForm';
import { userShape } from '../../shapes/index';


class AddRepairPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.saveRepair = this.saveRepair.bind(this);

    this.state = {
      errors: {},
      saving: false,
    };
  }

  saveRepair(repair) {
    this.setState({ saving: true, errors: {} });
    this.props.actions.createRepair(repair)
      .then(() => {
        this.setState({ saving: false });
        this.props.history.push('/');
        Alert.success('Repair created');
      })
      .catch((error) => {
        if (error.response && error.response.status === 422) {
          const message = error.response.data.message || 'Validation error';
          Alert.error(message);

          this.setState({ saving: false, errors: error.response.data.errors });
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    const { errors, saving } = this.state;
    const { users, usersLoading } = this.props;
    return (
      <Grid>
        <Row><h1>Add repair</h1></Row>
        <Row>
          <Col xs={8}>
            <RepairForm
              onSubmit={this.saveRepair}
              errors={errors}
              saving={saving}
              users={users}
              usersLoading={usersLoading}
              repair={{}}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

AddRepairPage.propTypes = {
  actions: PropTypes.shape({
    createRepair: PropTypes.func,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  users: PropTypes.arrayOf(userShape).isRequired,
  usersLoading: PropTypes.bool,
};

AddRepairPage.defaultProps = {
  usersLoading: false,
};

function mapStateToProps(state) {
  return {
    users: state.users.list,
    usersLoading: state.users.isFetching,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(repairActions, dispatch),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddRepairPage));
