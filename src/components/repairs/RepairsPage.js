import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Alert from 'react-s-alert';
import * as repairActions from '../../actions/repairActions';
import RepairsList from './RepairsList';
import { repairShape } from '../../shapes';

class RepairsPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.deleteRepair = this.deleteRepair.bind(this);
    this.editRepair = this.editRepair.bind(this);
    this.completeRepair = this.completeRepair.bind(this);
    this.dismissCompleteRepair = this.dismissCompleteRepair.bind(this);
    this.approveRepair = this.approveRepair.bind(this);
  }

  editRepair(id) {
    this.props.history.push(`/repairs/edit/${id}`);
  }

  deleteRepair(id) {
    return this.props.actions
      .deleteRepair(id)
      .then(() => {
        Alert.success('Repair deleted');
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 404) {
            Alert.error('Repair not found');
          } else if (error.response.data.message) {
            Alert.error(error.response.data.message);
          } else {
            Alert.error('Server error');
          }
        } else {
          Alert.error('Server error');
        }
      });
  }

  completeRepair(id) {
    this.props.actions.updateRepair({ state: 'Complete', id })
      .then(() => {
        Alert.success('Repair marked as complete and approved');
      })
      .catch((error) => {
        if (error.response) {
          const message = error.response.data.message || 'Repair Complete failed';
          Alert.error(message);
        } else {
          Alert.error('Server error');
        }
      });
  }

  dismissCompleteRepair(id) {
    this.props.actions.updateRepair({ state: 'Incomplete', id })
      .then(() => {
        Alert.success('Repair marked as incomplete');
      })
      .catch((error) => {
        if (error.response) {
          const message = error.response.data.message || 'Repair Incomplete failed';
          Alert.error(message);
        } else {
          Alert.error('Server error');
        }
      });
  }

  approveRepair(id) {
    this.props.actions.updateRepair({ approved: true, id })
      .then(() => {
        Alert.success('Repair approved');
      })
      .catch((error) => {
        if (error.response) {
          const message = error.response.data.message || 'Repair Approve failed';
          Alert.error(message);
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    return (
      <div>
        <h1>Repairs List</h1>
        <RepairsList
          {...this.props}
          onDelete={this.deleteRepair}
          onEdit={this.editRepair}
          onComplete={this.completeRepair}
          onDismissComplete={this.dismissCompleteRepair}
          onApprove={this.approveRepair}
        />
      </div>
    );
  }
}

RepairsPage.propTypes = {
  repairs: PropTypes.arrayOf(repairShape).isRequired,
  loading: PropTypes.bool,
  actions: PropTypes.shape({
    deleteRepair: PropTypes.func,
    updateRepair: PropTypes.func,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

RepairsPage.defaultProps = {
  loading: true,
};

function mapStateToProps(state) {
  return {
    repairs: state.repairs.list,
    loading: state.repairs.isFetching,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: Object.assign({},
      bindActionCreators(repairActions, dispatch),
    ),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RepairsPage));
