import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Alert from 'react-s-alert';
import * as repairActions from '../../actions/repairActions';
import UserRepairsList from './UserRepairsList';
import { repairShape } from '../../shapes';

class UserRepairsPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.completeRepair = this.completeRepair.bind(this);
  }

  completeRepair(id) {
    this.props.actions.updateRepair({ state: 'Complete', id })
      .then(() => {
        Alert.success('Repair marked as complete and has been sent on approval');
      })
      .catch((error) => {
        if (error.response) {
          const message = error.response.data.message || 'Repair Complete failed';
          Alert.error(message);
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    return (
      <div>
        <h1>Repairs List</h1>
        <UserRepairsList
          {...this.props}
          onComplete={this.completeRepair}
        />
      </div>
    );
  }
}

UserRepairsPage.propTypes = {
  repairs: PropTypes.arrayOf(repairShape).isRequired,
  loading: PropTypes.bool,
  actions: PropTypes.shape({
    updateRepair: PropTypes.func,
  }).isRequired,
};

UserRepairsPage.defaultProps = {
  loading: true,
};

function mapStateToProps(state) {
  return {
    repairs: state.repairs.list,
    loading: state.repairs.isFetching,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: Object.assign({},
      bindActionCreators(repairActions, dispatch),
    ),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserRepairsPage));
