import React from 'react';
import PropTypes from 'prop-types';
import {
  ButtonToolbar,
  OverlayTrigger,
  Button,
  Tooltip,
  Glyphicon,
} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { repairShape } from '../../shapes/index';

const tooltip = (id, text) => (
  <Tooltip id={id}>{text}</Tooltip>
);

class UserActionCell extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      loading: false,
    };

    this.onComplete = this.onComplete.bind(this);
  }

  onComplete() {
    this.props.onComplete(this.props.repair.id);
  }

  render() {
    const { repair } = this.props;
    const { loading } = this.state;

    return (
      <ButtonToolbar>
        <OverlayTrigger placement="top" overlay={tooltip('comments', 'View comments')}>
          <LinkContainer to={`/comments/${repair.id}`}>
            <Button
              bsStyle="default"
              bsSize="small"
              disabled={loading}
            ><Glyphicon glyph="comment" /></Button>
          </LinkContainer>
        </OverlayTrigger>

        {repair.state === 'Incomplete' && (
          <OverlayTrigger placement="top" overlay={tooltip('complete', 'Mark repair as complete')}>
            <Button
              bsStyle="success"
              bsSize="small"
              disabled={loading}
              onClick={this.onComplete}
            >
              <Glyphicon glyph="ok" />
            </Button>
          </OverlayTrigger>
        )}
      </ButtonToolbar>
    );
  }
}

UserActionCell.propTypes = {
  repair: repairShape.isRequired,
  onComplete: PropTypes.func.isRequired,
};

export default UserActionCell;
