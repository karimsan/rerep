import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Alert from 'react-s-alert';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import * as repairActions from '../../actions/repairActions';
import RepairForm from './RepairForm';
import { repairShape, userShape } from '../../shapes';


class EditRepairPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.saveRepair = this.saveRepair.bind(this);


    this.state = {
      errors: {},
      saving: false,
    };
  }

  componentDidMount() {
    this.props.actions.loadRepair(this.props.match.params.id)
      .catch(() => Alert.error('Load repair error'));
  }

  saveRepair(repair) {
    this.setState({ saving: true, errors: {} });
    this.props.actions.updateRepair(Object.assign({}, this.props.repair, repair))
      .then(() => {
        this.setState({ saving: false });
        this.props.history.push('/');
        Alert.success('Repair updated');
      })
      .catch((error) => {
        if (error.response && error.response.status === 422) {
          const message = error.response.data.message || 'Validation error';
          Alert.error(message);

          this.setState({ saving: false, errors: error.response.data.errors });
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    const { errors, saving } = this.state;
    const { users, usersLoading, repair, repairLoading } = this.props;
    return (
      <Grid>
        <Row><h1>Edit repair</h1></Row>
        <Row>
          <Col xs={8}>
            {repairLoading && (<h3>Loading...</h3>)}
            {!repairLoading && repair && (
              <RepairForm
                onSubmit={this.saveRepair}
                errors={errors}
                saving={saving}
                users={users}
                usersLoading={usersLoading}
                repair={repair}
              />
            )}
          </Col>
        </Row>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.users.list,
    usersLoading: state.users.isFetching,
    repair: state.repair.repair,
    repairLoading: state.repair.isFetching,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(repairActions, dispatch),
  };
}

EditRepairPage.propTypes = {
  actions: PropTypes.shape({
    loadRepair: PropTypes.func,
    updateRepair: PropTypes.func,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.objectOf(PropTypes.string),
  }).isRequired,
  users: PropTypes.arrayOf(userShape),
  repair: repairShape,
  usersLoading: PropTypes.bool,
  repairLoading: PropTypes.bool,
};

EditRepairPage.defaultProps = {
  users: [],
  repair: [],
  usersLoading: false,
  repairLoading: true,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditRepairPage));
