import React from 'react';
import PropTypes from 'prop-types';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Radio,
  Button,
  HelpBlock,
} from 'react-bootstrap';
import DateTime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { repairShape, userShape } from '../../shapes';

class RepairForm extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      assignee: (props.repair && props.repair.assignee) ? props.repair.assignee.id : null,
    };
    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.form = {};
  }

  onChange(assignee) {
    this.setState({ assignee });
  }

  handleSubmit(event) {
    event.preventDefault();
    const repair = {
      name: this.form.name.value,
      date: this.form.date.state.inputValue,
      state: this.form.stateComplete.checked ? 'Complete' : 'Incomplete',
      assignee_id: this.state.assignee,
    };
    this.props.onSubmit(repair);
  }

  render() {
    const { saving, errors, users, usersLoading, repair } = this.props;

    const options = users.map(user => ({ value: user.id, label: `${user.name} #${user.id}` }));

    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup
          controlId="name"
          validationState={errors.name ? 'error' : null}
        >
          <ControlLabel>Repair name</ControlLabel>
          <FormControl
            type="text"
            name="name"
            placeholder="Enter repair name"
            inputRef={(ref) => {
              this.form.name = ref;
            }}
            defaultValue={repair.name || null}
          />
          {errors.name && (
            <HelpBlock>{errors.name[0]}</HelpBlock>
          )}
        </FormGroup>
        <FormGroup
          controlId="state"
          validationState={errors.state ? 'error' : null}
        >
          <Radio
            name="state"
            inputRef={(ref) => {
              this.form.stateComplete = ref;
            }}
            defaultChecked={repair.state === 'Complete'}
          >
            Complete
          </Radio>
          <Radio
            name="state"
            inputRef={(ref) => {
              this.form.stateIncomplete = ref;
            }}
            defaultChecked={repair.state !== 'Complete'}
          >
            Incomplete
          </Radio>
          {errors.state && (
            <HelpBlock>{errors.state[0]}</HelpBlock>
          )}
        </FormGroup>
        <FormGroup
          controlId="date"
          validationState={errors.date ? 'error' : null}
        >
          <ControlLabel>Repair date and time</ControlLabel>
          <DateTime
            inputProps={{ name: 'date', placeholder: 'Enter date and time' }}
            dateFormat="YYYY-MM-DD"
            timeFormat="HH:mm"
            ref={(date) => {
              this.form.date = date;
            }}
            defaultValue={repair.date || ''}
            timeConstraints={{ minutes: { step: 15 } }}
          />
          {errors.date && (
            <HelpBlock>{errors.date[0]}</HelpBlock>
          )}
        </FormGroup>
        <FormGroup
          controlId="assignee_id"
          validationState={errors.assignee_id ? 'error' : null}
        >
          <ControlLabel>Assignee</ControlLabel>

          <Select
            onChange={this.onChange}
            options={options}
            isLoading={usersLoading}
            disabled={usersLoading}
            simpleValue
            value={this.state.assignee}
          />

          {errors.assignee_id && (
            <HelpBlock>{errors.assignee_id[0]}</HelpBlock>
          )}
        </FormGroup>
        <FormGroup>
          <Button
            type="submit"
            bsStyle="primary"
            disabled={saving}
          >
            {saving ? 'Saving...' : 'Save'}
          </Button>
        </FormGroup>
      </form>
    );
  }
}

RepairForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.objectOf(
    PropTypes.arrayOf(
      PropTypes.string,
    ),
  ),
  users: PropTypes.arrayOf(userShape),
  usersLoading: PropTypes.bool,
  repair: repairShape,
};

RepairForm.defaultProps = {
  saving: false,
  errors: {},
  users: [],
  repair: {},
  usersLoading: false,
};

export default RepairForm;
