import React from 'react';
import PropTypes from 'prop-types';
import {
  ButtonToolbar,
  OverlayTrigger,
  Button,
  Tooltip,
  Glyphicon,
} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { repairShape } from '../../shapes/index';

const tooltip = (id, text) => (
  <Tooltip id={id}>{text}</Tooltip>
);

class ActionCell extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onComplete = this.onComplete.bind(this);
    this.onDismissComplete = this.onDismissComplete.bind(this);
    this.onApprove = this.onApprove.bind(this);
  }

  onDelete() {
    if (window.confirm('Really delete repair?')) { // eslint-disable-line no-alert
      this.props.onDelete(this.props.repair.id);
    }
  }

  onEdit() {
    this.props.onEdit(this.props.repair.id);
  }

  onComplete() {
    this.props.onComplete(this.props.repair.id);
  }

  onDismissComplete() {
    this.props.onDismissComplete(this.props.repair.id);
  }

  onApprove() {
    this.props.onApprove(this.props.repair.id);
  }

  render() {
    const { repair, loading } = this.props;

    return (
      <ButtonToolbar>
        <OverlayTrigger placement="top" overlay={tooltip('comments', 'View comments')}>
          <LinkContainer to={`/comments/${repair.id}`}>
            <Button
              bsStyle="default"
              bsSize="small"
              disabled={loading}
            ><Glyphicon glyph="comment" /></Button>
          </LinkContainer>
        </OverlayTrigger>

        <OverlayTrigger placement="top" overlay={tooltip('edit', 'Edit repair')}>
          <Button
            bsStyle="primary"
            bsSize="small"
            disabled={loading}
            onClick={this.onEdit}
          ><Glyphicon glyph="pencil" /></Button>
        </OverlayTrigger>

        <OverlayTrigger placement="top" overlay={tooltip('remove', 'Delete repair')}>
          <Button
            bsStyle="danger"
            bsSize="small"
            disabled={loading}
            onClick={this.onDelete}
          ><Glyphicon glyph="trash" /></Button>
        </OverlayTrigger>

        {repair.state === 'Complete' && !repair.approved && (
          <OverlayTrigger placement="top" overlay={tooltip('approve', 'Approve repair')}>
            <Button
              bsStyle="success"
              bsSize="small"
              disabled={loading}
              onClick={this.onApprove}
            >
              <Glyphicon glyph="thumbs-up" />
            </Button>
          </OverlayTrigger>
        )}

        {repair.state === 'Incomplete' && (
          <OverlayTrigger placement="top" overlay={tooltip('complete', 'Mark repair as complete')}>
            <Button
              bsStyle="success"
              bsSize="small"
              disabled={loading}
              onClick={this.onComplete}
            >
              <Glyphicon glyph="ok" />
            </Button>
          </OverlayTrigger>
        )}

        {repair.state === 'Complete' && (
          <OverlayTrigger placement="top" overlay={tooltip('incomplete', 'Mark repair as incomplete')}>
            <Button
              bsStyle="warning"
              bsSize="small"
              disabled={loading}
              onClick={this.onDismissComplete}
            >
              <Glyphicon glyph="remove" />
            </Button>
          </OverlayTrigger>
        )}
      </ButtonToolbar>
    );
  }
}

ActionCell.propTypes = {
  repair: repairShape.isRequired,
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onComplete: PropTypes.func.isRequired,
  onDismissComplete: PropTypes.func.isRequired,
  onApprove: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default ActionCell;
