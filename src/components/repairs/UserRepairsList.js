import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import UserActionCell from './UserActionCell';
import { repairShape } from '../../shapes';

class UserRepairsList extends React.Component {
  constructor(props, context) {
    super(props, context);
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
        maxWidth: 40,
      },
      {
        Header: 'Title',
        accessor: 'name',
      },
      {
        Header: 'Date',
        accessor: 'date',
      },
      {
        Header: 'Time',
        accessor: 'time',
      },
      {
        Header: 'State',
        accessor: 'state',
        Filter: ({ filter, onChange }) =>
          (<select
            onChange={event => onChange(event.target.value)}
            style={{ width: '100%' }}
            value={filter ? filter.value : 'all'}
          >
            <option value="">Show All</option>
            <option value="Complete">Complete</option>
            <option value="Incomplete">Incomplete</option>
          </select>),
      },
      {
        id: 'approved',
        Header: 'Approved',
        accessor: d => ({ approved: d.approved, state: d.state }),
        Filter: ({ filter, onChange }) =>
          (<select
            onChange={event => onChange(event.target.value)}
            style={{ width: '100%' }}
            value={filter ? filter.value : 'all'}
          >
            <option value="">Show All</option>
            <option value="1">Approved</option>
            <option value="0">Pending</option>
          </select>),
        filterMethod: (filter, row) => {
          const { approved, state } = row.approved;
          if (filter.value === '1') {
            return approved;
          } else if (filter.value === '0') {
            return !approved && state === 'Complete';
          }
          return false;
        },
        Cell: (cellProps) => {
          const { approved, state } = cellProps.value;
          return (
            <span>
              {approved && 'Approved'}
              {!approved && state === 'Complete' && 'Pending'}
              {!approved && state !== 'Complete' && '-'}
            </span>);
        },
      },

      {
        id: 'actions',
        Header: 'Actions',
        filterable: false,
        accessor: d => d,
        Cell: cellProps => (<UserActionCell
          repair={cellProps.value}
          onComplete={this.props.onComplete}
        />),
      },
    ];
    this.state = { columns };
  }

  render() {
    const { repairs, loading } = this.props;

    return (
      <ReactTable
        data={repairs}
        loading={loading}
        columns={this.state.columns}
        filterable
      />
    );
  }
}

UserRepairsList.propTypes = {
  repairs: PropTypes.arrayOf(repairShape).isRequired,
  loading: PropTypes.bool,
  onComplete: PropTypes.func.isRequired,
};

UserRepairsList.defaultProps = {
  loading: true,
};

export default UserRepairsList;
