import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import ActionCell from './ActionCell';
import { repairShape } from '../../shapes';

class RepairsList extends React.Component {
  constructor(props, context) {
    super(props, context);
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
        width: 40,
      },
      {
        Header: 'Title',
        accessor: 'name',
      },
      {
        Header: 'Date',
        accessor: 'date',
        width: 100,
      },
      {
        Header: 'Time',
        accessor: 'time',
        width: 65,
      },
      {
        id: 'assigneeName',
        Header: 'Assignee',
        accessor: d => (d.assignee && d.assignee.name) || '-',
      },
      {
        Header: 'State',
        accessor: 'state',
        width: 100,
        Filter: ({ filter, onChange }) =>
          (<select
            onChange={event => onChange(event.target.value)}
            style={{ width: '100%' }}
            value={filter ? filter.value : 'all'}
          >
            <option value="">Show All</option>
            <option value="Complete">Complete</option>
            <option value="Incomplete">Incomplete</option>
          </select>),
      },
      {
        id: 'approved',
        Header: 'Approved',
        width: 100,
        accessor: d => ({ approved: d.approved, state: d.state }),
        Filter: ({ filter, onChange }) =>
          (<select
            onChange={event => onChange(event.target.value)}
            style={{ width: '100%' }}
            value={filter ? filter.value : 'all'}
          >
            <option value="">Show All</option>
            <option value="1">Approved</option>
            <option value="0">Pending</option>
          </select>),
        filterMethod: (filter, row) => {
          const { approved, state } = row.approved;
          if (filter.value === '1') {
            return approved;
          } else if (filter.value === '0') {
            return !approved && state === 'Complete';
          }
          return false;
        },
        Cell: (cellProps) => {
          const { approved, state } = cellProps.value;
          return (
            <span>
              {approved && 'Approved'}
              {!approved && state === 'Complete' && 'Pending'}
              {!approved && state !== 'Complete' && '-'}
            </span>);
        },
      },

      {
        id: 'actions',
        Header: 'Actions',
        filterable: false,
        accessor: d => d,
        width: 200,
        Cell: cellProps => (<ActionCell
          repair={cellProps.value}
          onDelete={this.props.onDelete}
          onEdit={this.props.onEdit}
          onComplete={this.props.onComplete}
          onDismissComplete={this.props.onDismissComplete}
          onApprove={this.props.onApprove}
          loading={this.props.loading}
        />),
      },
    ];
    this.state = { columns };
  }

  render() {
    const { repairs, loading } = this.props;

    return (
      <ReactTable
        data={repairs}
        loading={loading}
        columns={this.state.columns}
        filterable
      />
    );
  }
}

RepairsList.propTypes = {
  repairs: PropTypes.arrayOf(repairShape).isRequired,
  loading: PropTypes.bool,
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onComplete: PropTypes.func.isRequired,
  onDismissComplete: PropTypes.func.isRequired,
  onApprove: PropTypes.func.isRequired,
};

RepairsList.defaultProps = {
  loading: true,
};

export default RepairsList;
