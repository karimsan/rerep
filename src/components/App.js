import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { getToken } from '../token';
import * as authActions from '../actions/authActions';
import Header from './common/Header';
import AuthedApp from './AuthedApp';
import GuestApp from './GuestApp';
import '../styles/App.css';

class App extends React.Component {
  constructor(props, context) {
    super(props, context);

    const token = getToken();
    if (token) {
      axios.defaults.headers.common.Authorization = `Bearer ${token}`;
      this.state = { initialLoading: true };
      this.props.actions.me().then(() => {
        this.setState({ initialLoading: false });
      }).catch(() => {
        this.setState({ initialLoading: false });
      });
    } else {
      this.state = { initialLoading: false };
    }
  }

  render() {
    const { authed } = this.props;
    const { initialLoading } = this.state;
    return (
      <div>
        <Header />
        {initialLoading && (<h1>Loading...</h1>)}
        {!initialLoading && !authed && (<GuestApp />)}
        {!initialLoading && authed && (<AuthedApp />)}
      </div>
    );
  }
}

App.propTypes = {
  authed: PropTypes.bool.isRequired,
  actions: PropTypes.shape({
    me: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    authed: state.auth.user !== null,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: Object.assign({},
      bindActionCreators(authActions, dispatch),
    ),
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
