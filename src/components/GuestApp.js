import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Header from './common/Header';
import GuestNavbar from './auth/GuestNavbar';
import WelcomePage from './auth/WelcomePage';
import LoginPage from './auth/LoginPage';
import RegPage from './auth/RegPage';

function GuestApp() {
  return (
    <div>
      <Header />
      <GuestNavbar />
      <Switch>
        <Route exact path="/" component={WelcomePage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/register" component={RegPage} />
        <Redirect to="/" />
      </Switch>
    </div>
  );
}

export default GuestApp;
