import React from 'react';
import PropTypes from 'prop-types';
import { userShape } from '../../shapes/index';

function RoleContainer({ user, role, children }) {
  return (
    <div>
      {user.role === role && children}
    </div>
  );
}

RoleContainer.propTypes = {
  user: userShape.isRequired,
  children: PropTypes.node.isRequired,
  role: PropTypes.string.isRequired,
};

export default RoleContainer;
