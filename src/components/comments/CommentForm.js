import React from 'react';
import PropTypes from 'prop-types';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Button,
  HelpBlock,
} from 'react-bootstrap';

class CommentForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = { comment: '' };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ comment: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.onSubmit(this.state.comment).then(() => {
      this.setState({ comment: '' });
    });
  }

  render() {
    const { saving, errors } = this.props;
    const { comment } = this.state;

    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup
          controlId="comment"
          validationState={errors.comment ? 'error' : null}
        >
          <ControlLabel>New comment</ControlLabel>
          <FormControl
            componentClass="textarea"
            placeholder="Enter comment here"
            value={comment}
            onChange={this.handleChange}
          />
          {errors.comment && (
            <HelpBlock>{errors.comment[0]}</HelpBlock>
          )}
        </FormGroup>

        <FormGroup>
          <Button
            type="submit"
            bsStyle="primary"
            disabled={saving}
          >
            {saving ? 'Send...' : 'Send'}
          </Button>
        </FormGroup>
      </form>
    );
  }
}

CommentForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.objectOf(
    PropTypes.arrayOf(
      PropTypes.string,
    ),
  ),
};

CommentForm.defaultProps = {
  saving: false,
  errors: {},
};

export default CommentForm;
