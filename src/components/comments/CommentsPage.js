import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Alert from 'react-s-alert';
import {
  Grid,
  Row,
} from 'react-bootstrap';
import * as repairActions from '../../actions/repairActions';
import * as commentActions from '../../actions/commentActions';
import { commentShape, repairShape } from '../../shapes/index';
import CommentForm from './CommentForm';
import CommentsList from './CommentsList';


class CommentsPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.saveComment = this.saveComment.bind(this);

    this.state = {
      errors: {},
      saving: false,
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.actions.loadComments(id)
      .catch(() => Alert.error('Load comments error'));
    this.props.actions.loadRepair(id)
      .catch(() => Alert.error('Load repair error'));
  }

  saveComment(comment) {
    this.setState({ saving: true, errors: {} });
    return this.props.actions.createComment(this.props.repair.id, comment)
      .then(() => {
        this.setState({ saving: false });
        Alert.success('Comment sent');
      })
      .catch((error) => {
        if (error.response && error.response.status === 422) {
          const message = error.response.data.message || 'Validation error';
          Alert.error(message);

          this.setState({ saving: false, errors: error.response.data.errors });
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    const { errors, saving } = this.state;
    const { repair, comments, loading } = this.props;
    return (
      <Grid>

        <Row><h1>Comments</h1>
          {loading && (<h3>Loading...</h3>)}
          {!loading && (<h3>Repair &quot;{repair.name}&quot;</h3>)}
        </Row>

        <Row>
          {!loading && (
            <CommentForm
              onSubmit={this.saveComment}
              errors={errors}
              saving={saving}
            />
          )}
        </Row>

        <Row>
          {!loading && (
            <CommentsList comments={comments} />
          )}
        </Row>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.comments.isFetching || state.repair.isFetching,
    comments: state.comments.list,
    repair: state.repair.repair,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: Object.assign({},
      bindActionCreators(commentActions, dispatch),
      bindActionCreators(repairActions, dispatch),
    ),
  };
}

CommentsPage.propTypes = {
  actions: PropTypes.shape({
    loadComments: PropTypes.func,
    createComment: PropTypes.func,
    loadRepair: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.objectOf(PropTypes.string),
  }).isRequired,
  comments: PropTypes.arrayOf(commentShape),
  loading: PropTypes.bool,
  repair: repairShape,
};

CommentsPage.defaultProps = {
  comments: [],
  loading: true,
  repair: {},
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CommentsPage));
