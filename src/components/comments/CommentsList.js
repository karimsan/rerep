import React from 'react';
import PropType from 'prop-types';
import { Panel } from 'react-bootstrap';
import moment from 'moment';
import { commentShape } from '../../shapes/index';


function header(user, date) {
  return (
    <span>
      <strong>{user.name}</strong> commented {moment(date).fromNow()}
    </span>
  );
}

function CommentsList({ comments }) {
  return (
    <div>
      {comments.map(comment => (
        <Panel key={comment.id} header={header(comment.user, comment.created_at)}>
          {comment.comment}
        </Panel>
      ))}
    </div>
  );
}

CommentsList.propTypes = {
  comments: PropType.arrayOf(commentShape).isRequired,
};

export default CommentsList;
