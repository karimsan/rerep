import React from 'react';
import {
  Navbar,
  Nav,
  NavItem,
} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';

function GuestNavbar() {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <NavLink to="/" exact>Home</NavLink>
        </Navbar.Brand>
      </Navbar.Header>
      <Nav>
        <LinkContainer to="/login"><NavItem>Login</NavItem></LinkContainer>
        <LinkContainer to="/register"><NavItem>Register</NavItem></LinkContainer>
      </Nav>
    </Navbar>
  );
}

export default GuestNavbar;
