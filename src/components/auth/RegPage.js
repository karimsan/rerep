import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Alert from 'react-s-alert';
import { Grid, Row, Col } from 'react-bootstrap';
import * as authActions from '../../actions/authActions';
import { userShape } from '../../shapes/index';
import RegForm from './RegForm';

class RegPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      errors: {},
    };

    this.regUser = this.regUser.bind(this);
    this.onRegSuccess = this.onRegSuccess.bind(this);
  }

  onRegSuccess() {
    Alert.success('Authenticated');
    this.props.history.push('/');
  }

  regUser(user) {
    return this.props.actions
      .register(user, this.onRegSuccess)
      .catch((error) => {
        if (error.response && error.response.status === 422) {
          const message = error.response.data.message || 'Validation error';
          Alert.error(message);

          this.setState({ errors: error.response.data.errors });
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    const { errors } = this.state;
    return (
      <Grid>
        <Row>
          <Col xs={12} md={8} mdOffset={2}>
            <RegForm
              {...this.props}
              onSubmit={this.regUser}
              errors={errors}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

RegPage.propTypes = {
  user: userShape,
  loading: PropTypes.bool,
  actions: PropTypes.shape({
    register: PropTypes.func,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

RegPage.defaultProps = {
  user: null,
  loading: false,
};

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    loading: state.auth.isFetching,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RegPage));
