import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Alert from 'react-s-alert';
import { Grid, Row, Col } from 'react-bootstrap';
import * as authActions from '../../actions/authActions';
import { userShape } from '../../shapes/index';
import AuthForm from './AuthForm';

class LoginPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.authUser = this.authUser.bind(this);
    this.onAuthSuccess = this.onAuthSuccess.bind(this);
  }

  onAuthSuccess() {
    Alert.success('Authenticated');
    this.props.history.push('/');
  }

  authUser(user) {
    return this.props.actions
      .login(user, this.onAuthSuccess)
      .catch((error) => {
        if (error.response) {
          if (error.response.data.message) {
            Alert.error(error.response.data.message);
          } else {
            Alert.error('Auth error');
          }
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col xs={12} md={8} mdOffset={2}>
            <AuthForm
              {...this.props}
              onSubmit={this.authUser}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

LoginPage.propTypes = {
  user: userShape,
  loading: PropTypes.bool,
  actions: PropTypes.shape({
    login: PropTypes.func,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

LoginPage.defaultProps = {
  user: null,
  loading: false,
};

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    loading: state.auth.isFetching,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginPage));
