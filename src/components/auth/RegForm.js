import React from 'react';
import PropTypes from 'prop-types';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Button,
  HelpBlock,
} from 'react-bootstrap';
import 'react-datetime/css/react-datetime.css';
import 'react-select/dist/react-select.css';

class RegForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.form = {};
  }

  handleSubmit(event) {
    event.preventDefault();
    const newUser = {
      name: this.form.name.value,
      email: this.form.email.value,
      password: this.form.password.value,
    };
    this.props.onSubmit(newUser);
  }

  render() {
    const { loading, errors } = this.props;

    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup
          controlId="name"
          validationState={errors.name ? 'error' : null}
        >
          <ControlLabel>Name</ControlLabel>
          <FormControl
            type="text"
            name="name"
            placeholder="Enter your name"
            inputRef={(ref) => {
              this.form.name = ref;
            }}
          />
          {errors.name && (
            <HelpBlock>{errors.name[0]}</HelpBlock>
          )}
        </FormGroup>
        <FormGroup
          controlId="email"
          validationState={errors.email ? 'error' : null}
        >
          <ControlLabel>Email</ControlLabel>
          <FormControl
            type="text"
            name="email"
            placeholder="Enter email"
            inputRef={(ref) => {
              this.form.email = ref;
            }}
          />
          {errors.email && (
            <HelpBlock>{errors.email[0]}</HelpBlock>
          )}
        </FormGroup>

        <FormGroup
          controlId="password"
          validationState={errors.password ? 'error' : null}
        >
          <ControlLabel>Password</ControlLabel>
          <FormControl
            type="password"
            name="password"
            placeholder="Enter password"
            inputRef={(ref) => {
              this.form.password = ref;
            }}
          />
          {errors.password && (
            <HelpBlock>{errors.password[0]}</HelpBlock>
          )}
        </FormGroup>

        <FormGroup>
          <Button
            type="submit"
            bsStyle="primary"
            disabled={loading}
          >
            {loading ? '...' : 'Register'}
          </Button>
        </FormGroup>
      </form>
    );
  }
}

RegForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  errors: PropTypes.objectOf(
    PropTypes.arrayOf(
      PropTypes.string,
    ),
  ),
};

RegForm.defaultProps = {
  loading: false,
  errors: {},
};

export default RegForm;
