import React from 'react';
import PropTypes from 'prop-types';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Button,
  HelpBlock,
} from 'react-bootstrap';
import 'react-datetime/css/react-datetime.css';
import 'react-select/dist/react-select.css';

class AuthForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.form = {};
  }

  handleSubmit(event) {
    event.preventDefault();
    const user = {
      email: this.form.email.value,
      password: this.form.password.value,
    };
    this.props.onSubmit(user);
  }

  render() {
    const { loading, errors } = this.props;

    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup
          controlId="email"
          validationState={errors.email ? 'error' : null}
        >
          <ControlLabel>Email</ControlLabel>
          <FormControl
            type="text"
            name="email"
            placeholder="Enter email"
            inputRef={(ref) => {
              this.form.email = ref;
            }}
          />
          {errors.email && (
            <HelpBlock>{errors.email[0]}</HelpBlock>
          )}
        </FormGroup>

        <FormGroup
          controlId="password"
          validationState={errors.password ? 'error' : null}
        >
          <ControlLabel>Password</ControlLabel>
          <FormControl
            type="password"
            name="password"
            placeholder="Enter password"
            inputRef={(ref) => {
              this.form.password = ref;
            }}
          />
          {errors.password && (
            <HelpBlock>{errors.password[0]}</HelpBlock>
          )}
        </FormGroup>

        <FormGroup>
          <Button
            type="submit"
            bsStyle="primary"
            disabled={loading}
          >
            {loading ? '...' : 'Login'}
          </Button>
        </FormGroup>
      </form>
    );
  }
}

AuthForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  errors: PropTypes.objectOf(
    PropTypes.arrayOf(
      PropTypes.string,
    ),
  ),
};

AuthForm.defaultProps = {
  loading: false,
  errors: {},
};

export default AuthForm;
