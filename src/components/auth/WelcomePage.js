import React from 'react';
import {
  Grid,
  Jumbotron,
  Button,
  ButtonToolbar,
} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

function WelcomePage() {
  return (
    <Jumbotron>
      <Grid>
        <h1>Welcome to Repair Shop</h1>
        <p>Join and enjoy!</p>
        <ButtonToolbar>
          <LinkContainer to="/login">
            <Button bsStyle="success" bsSize="large">
              Login and start
            </Button>
          </LinkContainer>
          <LinkContainer to="/register">
            <Button bsStyle="default" bsSize="large">
              Create new account
            </Button>
          </LinkContainer>
        </ButtonToolbar>
      </Grid>
    </Jumbotron>
  );
}

export default WelcomePage;
