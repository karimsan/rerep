import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import ActionCell from './ActionCell';
import { userShape } from '../../shapes';

class UsersList extends React.Component {
  constructor(props, context) {
    super(props, context);
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
        maxWidth: 40,
      },
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Email',
        accessor: 'email',
      },
      {
        Header: 'Role',
        accessor: 'role',
        Filter: ({ filter, onChange }) =>
          (<select
            onChange={event => onChange(event.target.value)}
            style={{ width: '100%' }}
            value={filter ? filter.value : 'all'}
          >
            <option value="">Show All</option>
            <option value="user">User</option>
            <option value="manager">Manager</option>
          </select>),
      },
      {
        id: 'actions',
        Header: 'Actions',
        filterable: false,
        accessor: d => d,
        Cell: cellProps => (<ActionCell
          user={cellProps.value}
          onDelete={this.props.onDelete}
          onEdit={this.props.onEdit}
          loading={this.props.loading}
        />),
      },
    ];
    this.state = { columns };
  }

  render() {
    const { users, loading } = this.props;

    return (
      <ReactTable
        data={users}
        loading={loading}
        columns={this.state.columns}
        filterable
      />
    );
  }
}

UsersList.propTypes = {
  users: PropTypes.arrayOf(userShape).isRequired,
  loading: PropTypes.bool,
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
};

UsersList.defaultProps = {
  loading: true,
};

export default UsersList;
