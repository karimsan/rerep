import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Alert from 'react-s-alert';
import * as userActions from '../../actions/userActions';
import UsersList from './UsersList';
import { userShape } from '../../shapes';

class UsersPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.deleteUser = this.deleteUser.bind(this);
    this.editUser = this.editUser.bind(this);
  }

  editUser(id) {
    this.props.history.push(`/users/edit/${id}`);
  }

  deleteUser(id) {
    if (id === this.props.me.id) {
      Alert.error('Delete anybody but not yourself');
      return null;
    }
    return this.props.actions
      .deleteUser(id)
      .then(() => {
        Alert.success('User deleted');
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 404) {
            Alert.error('User not found');
          } else if (error.response.data.message) {
            Alert.error(error.response.data.message);
          } else {
            Alert.error('Server error');
          }
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    return (
      <UsersList
        {...this.props}
        onDelete={this.deleteUser}
        onEdit={this.editUser}
      />
    );
  }
}

UsersPage.propTypes = {
  users: PropTypes.arrayOf(userShape).isRequired,
  loading: PropTypes.bool,
  actions: PropTypes.shape({
    deleteUser: PropTypes.func,
    updateUser: PropTypes.func,
  }).isRequired,
  me: userShape.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

UsersPage.defaultProps = {
  loading: true,
};

function mapStateToProps(state) {
  return {
    users: state.users.list,
    loading: state.users.isFetching,
    me: state.auth.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: Object.assign({},
      bindActionCreators(userActions, dispatch),
    ),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UsersPage));
