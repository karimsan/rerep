import React from 'react';
import PropTypes from 'prop-types';
import 'react-table/react-table.css';
import {
  ButtonToolbar,
  OverlayTrigger,
  Button,
  Tooltip,
  Glyphicon,
} from 'react-bootstrap';
import { userShape } from '../../shapes/index';

const tooltip = (id, text) => (
  <Tooltip id={id}>{text}</Tooltip>
);

class ActionCell extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }

  onDelete() {
    if (window.confirm('Really delete user?')) { // eslint-disable-line no-alert
      this.props.onDelete(this.props.user.id);
    }
  }

  onEdit() {
    this.props.onEdit(this.props.user.id);
  }

  render() {
    const { loading } = this.props;

    return (
      <ButtonToolbar>
        <OverlayTrigger placement="top" overlay={tooltip('edit', 'Edit user')}>
          <Button
            bsStyle="primary"
            bsSize="small"
            disabled={loading}
            onClick={this.onEdit}
          ><Glyphicon glyph="pencil" /></Button>
        </OverlayTrigger>

        <OverlayTrigger placement="top" overlay={tooltip('remove', 'Delete user')}>
          <Button
            bsStyle="danger"
            bsSize="small"
            disabled={loading}
            onClick={this.onDelete}
          ><Glyphicon glyph="trash" /></Button>
        </OverlayTrigger>


      </ButtonToolbar>
    );
  }
}

ActionCell.propTypes = {
  user: userShape.isRequired,
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default ActionCell;
