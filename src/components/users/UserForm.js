import React from 'react';
import PropTypes from 'prop-types';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Radio,
  Button,
  HelpBlock,
} from 'react-bootstrap';
import { userShape } from '../../shapes';

class UserForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.form = {};
  }

  handleSubmit(event) {
    event.preventDefault();

    let role = null;
    if (this.form.roleManager.checked) {
      role = 'manager';
    } else if (this.form.roleUser.checked) {
      role = 'user';
    }

    const user = {
      name: this.form.name.value,
      email: this.form.email.value,
      password: this.form.password.value,
      role,
    };
    this.props.onSubmit(user);
  }

  render() {
    const { saving, errors, user } = this.props;

    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup
          controlId="name"
          validationState={errors.name ? 'error' : null}
        >
          <ControlLabel>User name</ControlLabel>
          <FormControl
            type="text"
            name="name"
            placeholder="Enter user name"
            inputRef={(ref) => {
              this.form.name = ref;
            }}
            defaultValue={user.name || null}
          />
          {errors.name && (
            <HelpBlock>{errors.name[0]}</HelpBlock>
          )}
        </FormGroup>

        <FormGroup
          controlId="email"
          validationState={errors.email ? 'error' : null}
        >
          <ControlLabel>Email</ControlLabel>
          <FormControl
            type="text"
            name="email"
            placeholder="Enter user email"
            inputRef={(ref) => {
              this.form.email = ref;
            }}
            defaultValue={user.email || null}
          />
          {errors.email && (
            <HelpBlock>{errors.email[0]}</HelpBlock>
          )}
        </FormGroup>

        <FormGroup
          controlId="password"
          validationState={errors.password ? 'error' : null}
        >
          <ControlLabel>User password</ControlLabel>
          <FormControl
            type="password"
            name="password"
            placeholder="Enter password"
            inputRef={(ref) => {
              this.form.password = ref;
            }}
          />
          {errors.password && (
            <HelpBlock>{errors.password[0]}</HelpBlock>
          )}
          {!errors.password && user.id > 0 && (
            <HelpBlock>Leave blank to save old value</HelpBlock>
          )}
        </FormGroup>

        <FormGroup
          controlId="role"
          validationState={errors.role ? 'error' : null}
        >
          <Radio
            name="role"
            inputRef={(ref) => {
              this.form.roleUser = ref;
            }}
            defaultChecked={user.role === 'user'}
          >
            User
          </Radio>
          <Radio
            name="role"
            inputRef={(ref) => {
              this.form.roleManager = ref;
            }}
            defaultChecked={user.role === 'manager'}
          >
            Manager
          </Radio>
          {errors.role && (
            <HelpBlock>{errors.role[0]}</HelpBlock>
          )}
        </FormGroup>

        <FormGroup>
          <Button
            type="submit"
            bsStyle="primary"
            disabled={saving}
          >
            {saving ? 'Saving...' : 'Save'}
          </Button>
        </FormGroup>
      </form>
    );
  }
}

UserForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.objectOf(
    PropTypes.arrayOf(
      PropTypes.string,
    ),
  ),
  user: userShape,
};

UserForm.defaultProps = {
  saving: false,
  errors: {},
  user: {},
};

export default UserForm;
