import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Alert from 'react-s-alert';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import * as userActions from '../../actions/userActions';
import UserForm from './UserForm';


class AddUserPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.saveUser = this.saveUser.bind(this);

    this.state = {
      errors: {},
      saving: false,
    };
  }

  saveUser(user) {
    this.setState({ saving: true, errors: {} });
    this.props.actions.createUser(user)
      .then(() => {
        this.setState({ saving: false });
        this.props.history.push('/users');
        Alert.success('User created');
      })
      .catch((error) => {
        if (error.response && error.response.status === 422) {
          const message = error.response.data.message || 'Validation error';
          Alert.error(message);

          this.setState({ saving: false, errors: error.response.data.errors });
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    const { errors, saving } = this.state;
    return (
      <Grid>
        <Row><h1>Add user</h1></Row>
        <Row>
          <Col xs={8}>
            <UserForm
              onSubmit={this.saveUser}
              errors={errors}
              saving={saving}
              user={{}}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

AddUserPage.propTypes = {
  actions: PropTypes.shape({
    createUser: PropTypes.func,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};


function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddUserPage));
