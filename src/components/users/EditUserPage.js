import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Alert from 'react-s-alert';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import * as userActions from '../../actions/userActions';
import UserForm from './UserForm';
import { userShape } from '../../shapes';


class EditUserPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.saveUser = this.saveUser.bind(this);

    this.state = {
      errors: {},
      saving: false,
    };
  }

  componentDidMount() {
    this.props.actions.loadUser(this.props.match.params.id)
      .catch(() => Alert.error('Load user error'));
  }

  saveUser(user) {
    this.setState({ saving: true, errors: {} });
    this.props.actions.updateUser(Object.assign({}, this.props.user, user))
      .then(() => {
        this.setState({ saving: false });
        this.props.history.push('/users');
        Alert.success('User updated');
      })
      .catch((error) => {
        if (error.response && error.response.status === 422) {
          const message = error.response.data.message || 'Validation error';
          Alert.error(message);
          this.setState({ saving: false, errors: error.response.data.errors });
        } else {
          Alert.error('Server error');
        }
      });
  }

  render() {
    const { errors, saving } = this.state;
    const { user, userLoading } = this.props;
    return (
      <Grid>
        <Row><h1>Edit user</h1></Row>
        <Row>
          <Col xs={8}>
            {userLoading && (<h3>Loading...</h3>)}
            {!userLoading && user && (
              <UserForm
                onSubmit={this.saveUser}
                errors={errors}
                saving={saving}
                user={user}
              />
            )}
          </Col>
        </Row>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user.user,
    userLoading: state.user.isFetching,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch),
  };
}

EditUserPage.propTypes = {
  actions: PropTypes.shape({
    loadUser: PropTypes.func,
    updateUser: PropTypes.func,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.objectOf(PropTypes.string),
  }).isRequired,
  user: userShape,
  userLoading: PropTypes.bool,
};

EditUserPage.defaultProps = {
  user: {},
  userLoading: true,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditUserPage));
