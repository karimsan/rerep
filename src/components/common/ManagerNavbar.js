import React from 'react';
import PropType from 'prop-types';
import {
  Navbar,
  Nav,
  NavItem,
} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';

function ManagerNavbar({ logout }) {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <NavLink to="/" exact>Home</NavLink>
        </Navbar.Brand>
      </Navbar.Header>
      <Nav>
        <LinkContainer to="/repairs/add"><NavItem>Add Repair</NavItem></LinkContainer>
        <LinkContainer to="/users" exact><NavItem>Manage Users</NavItem></LinkContainer>
        <LinkContainer to="/users/add"><NavItem>Add User</NavItem></LinkContainer>
      </Nav>
      <Nav pullRight>
        <NavItem onClick={logout}>Logout</NavItem>
      </Nav>
    </Navbar>
  );
}

ManagerNavbar.propTypes = {
  logout: PropType.func.isRequired,
};

export default ManagerNavbar;
