import React from 'react';

import LoadingBar from 'react-redux-loading-bar';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/stackslide.css';

function Header() {
  return (
    <div>
      <Alert stack={{ limit: 1 }} position="top" effect="stackslide" />
      <LoadingBar style={{ zIndex: 1 }} />
    </div>
  );
}

export default Header;
