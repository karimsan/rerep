import React from 'react';
import PropType from 'prop-types';
import {
  Navbar,
  Nav,
  NavItem,
} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

function UserNavbar({ logout }) {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <NavLink to="/" exact>Home</NavLink>
        </Navbar.Brand>
      </Navbar.Header>
      <Nav pullRight>
        <NavItem onClick={logout}>Logout</NavItem>
      </Nav>
    </Navbar>
  );
}

UserNavbar.propTypes = {
  logout: PropType.func.isRequired,
};

export default UserNavbar;
