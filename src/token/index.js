const TOKEN = 'TOKEN';

function getToken() {
  try {
    return window.localStorage.getItem(TOKEN);
  } catch (e) {
    return null;
  }
}

function setToken(token) {
  try {
    return window.localStorage.setItem(TOKEN, token);
  } catch (e) {
    return null;
  }
}

function cleanToken() {
  try {
    return window.localStorage.removeItem(TOKEN);
  } catch (e) {
    return null;
  }
}

export {
  getToken,
  setToken,
  cleanToken,
};
