import PropTypes from 'prop-types';

const userShape = PropTypes.shape({
  id: PropTypes.number,
  name: PropTypes.string,
  email: PropTypes.string,
  role: PropTypes.string,
});

const repairShape = PropTypes.shape({
  id: PropTypes.number,
  name: PropTypes.string,
  state: PropTypes.string,
  approved: PropTypes.bool,
  date: PropTypes.string,
  assignee: userShape,
});

const commentShape = PropTypes.shape({
  id: PropTypes.number,
  comment: PropTypes.string,
  created_at: PropTypes.string,
  user: userShape,
});


export {
  userShape,
  repairShape,
  commentShape,
};
