export const REPAIRS_LOAD_LIST = 'REPAIRS_LOAD_LIST';
export const REPAIR_LOAD_ONE = 'REPAIR_LOAD_ONE';
export const REPAIR_CREATE = 'REPAIR_CREATE';
export const REPAIR_UPDATE = 'REPAIR_UPDATE';
export const REPAIR_DELETE = 'REPAIR_DELETE';

export const USERS_LOAD_LIST = 'USERS_LOAD_LIST';
export const USER_LOAD_ONE = 'USER_LOAD_ONE';
export const USER_CREATE = 'USER_CREATE';
export const USER_UPDATE = 'USER_UPDATE';
export const USER_DELETE = 'USER_DELETE';

export const REGISTER = 'REGISTER';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const ME = 'ME';

export const COMMENTS_LOAD = 'COMMENTS_LOAD';
export const COMMENTS_CREATE = 'COMMENTS_CREATE';
