import * as types from './actionTypes';
import LoginApi from '../api/LoginApi';

export const me = () => ({
  type: types.ME,
  payload: LoginApi.me(),
});

export const login = (user, cb) => dispatch => dispatch({
  type: types.LOGIN,
  payload: LoginApi.login(user),
}).then(() => {
  dispatch(me()).then(() => {
    cb();
  });
});

export const register = (newUser, cb) => dispatch => dispatch({
  type: types.REGISTER,
  payload: LoginApi.register(newUser),
}).then(() => {
  dispatch(me()).then(() => {
    cb();
  });
});

export const logout = () => ({
  type: types.LOGOUT,
});
