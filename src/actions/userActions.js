import * as types from './actionTypes';
import UserApi from '../api/UserApi';

export const loadUsers = () => ({
  type: types.USERS_LOAD_LIST,
  payload: UserApi.getAllUsers(),
});

export const createUser = user => ({
  type: types.USER_CREATE,
  payload: UserApi.create(user),
});

export const updateUser = user => ({
  type: types.USER_UPDATE,
  payload: UserApi.update(user),
  meta: { id: user.id },
});

export const loadUser = id => ({
  type: types.USER_LOAD_ONE,
  payload: UserApi.getOne(id),
  meta: { id },
});

export const deleteUser = id => ({
  type: types.USER_DELETE,
  payload: UserApi.delete(id),
  meta: { id },
});
