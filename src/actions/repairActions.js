import * as types from './actionTypes';
import RepairApi from '../api/RepairApi';

export const loadRepairs = () => ({
  type: types.REPAIRS_LOAD_LIST,
  payload: RepairApi.getAllRepairs(),
});

export const createRepair = repair => ({
  type: types.REPAIR_CREATE,
  payload: RepairApi.create(repair),
});

export const updateRepair = repair => ({
  type: types.REPAIR_UPDATE,
  payload: RepairApi.update(repair),
  meta: { id: repair.id },
});

export const loadRepair = id => ({
  type: types.REPAIR_LOAD_ONE,
  payload: RepairApi.getOne(id),
  meta: { id },
});

export const deleteRepair = id => ({
  type: types.REPAIR_DELETE,
  payload: RepairApi.delete(id),
  meta: { id },
});
