import * as types from './actionTypes';
import CommentApi from '../api/CommentApi';

export const loadComments = repairId => ({
  type: types.COMMENTS_LOAD,
  payload: CommentApi.get(repairId),
});

export const createComment = (repairID, comment) => ({
  type: types.COMMENTS_CREATE,
  payload: CommentApi.create(repairID, comment),
});
