import axios from 'axios';

class CommentApi {
  static get(repairId) {
    return axios.get(`comments/${repairId}`);
  }

  static create(repairId, comment) {
    return axios.post(`comments/${repairId}`, { comment });
  }
}

export default CommentApi;
