import axios from 'axios';

class LoginApi {
  static login(user) {
    return axios.post('auth/login', user);
  }

  static me() {
    return axios.get('auth/me');
  }

  static register(newUser) {
    return axios.post('auth/register', newUser);
  }
}

export default LoginApi;
