import axios from 'axios';

class RepairApi {
  static getAllRepairs() {
    return axios.get('repairs');
  }

  static getOne(id) {
    return axios.get(`repairs/${id}`);
  }

  static create(repair) {
    return axios.post('repairs', repair);
  }

  static update(repair) {
    return axios.put(`repairs/${repair.id}`, repair);
  }

  static delete(id) {
    return axios.delete(`repairs/${id}`);
  }
}

export default RepairApi;
