import axios from 'axios';

class UserApi {
  static getAllUsers() {
    return axios.get('users');
  }

  static getOne(id) {
    return axios.get(`users/${id}`);
  }

  static create(user) {
    return axios.post('users', user);
  }

  static update(user) {
    return axios.put(`users/${user.id}`, user);
  }

  static delete(id) {
    return axios.delete(`users/${id}`);
  }
}

export default UserApi;
